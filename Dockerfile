FROM saltstack/centos-6-minimal
USER root
RUN yum install -y openscap-scanner; exit 0
RUN yum install -y xml-common
RUN rpm -ivh http://mirror.centos.org/centos/7/os/x86_64/Packages/scap-security-guide-0.1.40-12.el7.centos.0.1.noarch.rpm
RUN oscap xccdf eval --remediate --profile xccdf_org.ssgproject.content_profile_stig-rhel6-disa --results-arf arf.xml --report report.html /usr/share/xml/scap/ssg/content/ssg-centos6-ds.xml; exit 0
